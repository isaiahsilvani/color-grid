package com.example.colorappv6

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.colorappv6.databinding.FragmentGridBinding
import kotlinx.coroutines.selects.select


class GridFragment: Fragment() {

    lateinit var binding: FragmentGridBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentGridBinding.inflate(inflater, container, false)
        println("CREATED!!!")
        initViews()
        return binding.root
    }

    fun initViews() {
        with(binding) {
            // Find a way to do this by getting children
            // of the grid instead of hard coding
            val buttons: List<Button> = listOf(darkBlue, darkGreen, darkPurple, firetruckRed, lightRed,
                blue, skyBlue, pink, black, white, orange, lightRed, lightGreen, lightOrange, green, cyan,
            yellow)

            for (button in buttons) {
                button.setOnClickListener {
                    val tag = button.tag.toString()
                    val action = GridFragmentDirections
                        .actionGridFragmentToColorFragment(selectedColor = tag)

                    findNavController().navigate(action)
                }
            }
        }

    }

}