package com.example.colorappv6

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.colorappv6.databinding.FragmentOneColorBinding

class ColorFragment : Fragment() {

    lateinit var binding: FragmentOneColorBinding
    val args by navArgs<ColorFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentOneColorBinding.inflate(inflater, container, false)

        with(binding) {
            // binding.button.setBackgroundColor()
            val color = args.selectedColor
            println("HEX CODE IS ${color}")

            val parseColor = Color.parseColor(color)
//        binding.button.setTextColor(parseColor)

            parseColor
            button.backgroundTintList = ColorStateList.valueOf(parseColor)

            button.text = color

            println(button.background)
            initViews()
            return root
        }


    }

    fun initViews() {
        with(binding) {

            button.setOnClickListener {
                val action = ColorFragmentDirections
                    .actionColorFragmentToGridFragment()

                findNavController().navigate(action)
            }
        }
    }

}